import { Draft, PayloadAction } from '@reduxjs/toolkit';
import { AnyObject, ActionMeta, DataState } from '../';
export declare function manyToArr(state: Draft<DataState<AnyObject[]>>, action: PayloadAction<AnyObject[], string, ActionMeta, boolean>): void;
export declare function manyToObj(state: Draft<DataState<AnyObject>>, action: PayloadAction<AnyObject[], string, ActionMeta, boolean>): void;
export declare function oneToArr(state: Draft<DataState<AnyObject[]>>, action: PayloadAction<AnyObject, string, ActionMeta, boolean>): void;
export declare function oneToObj(state: Draft<DataState<AnyObject>>, action: PayloadAction<AnyObject, string, ActionMeta, boolean>): void;
