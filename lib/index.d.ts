export { Draft } from '@reduxjs/toolkit';
export * from './fetchHandler';
export * from './makeSlice';
export * from './createRepo';
export declare enum ReducerOp {
    Update = "Update",
    Delete = "Delete"
}
export declare type Dictionary<T> = Record<string, T>;
export declare type AnyObject = Dictionary<any>;
export interface DataState<T> {
    data: T;
    status: 'idle' | 'pending';
    error?: Error;
    received?: number;
    requested?: number;
}
export declare type ActionMeta = {
    targetId?: string;
    targetIds?: string[];
    idKey?: string;
};
export declare function unpack<T>(dataState: DataState<T>): T;
declare type Processor = <Result>(fetchResult: unknown) => Result;
declare type ErrorHandler = (err: Error) => void;
export declare type ReduxIrlConfig = {
    /**
     * Will receive the result of a repo's fetch call. Return value will be passed to reducers.
     */
    defaultProcessor?: Processor;
    /**
     * Intercept fetch errors. A `/setFailed` action will still be fired to set the error property of `EntityState` in the
     * store. It might still be useful to handle all errors centrally.
     */
    errorHandler?: ErrorHandler;
};
declare class ReduxIrl {
    defaultProcessor: Processor;
    errorHandler: ErrorHandler;
    init({ defaultProcessor, errorHandler, }: ReduxIrlConfig): void;
}
declare const reduxIrl: ReduxIrl;
export default reduxIrl;
