import { AnyObject, DataState } from '.';
export declare function isDataState(test: any): test is DataState<unknown>;
export declare function isArrayDataState(test: any): test is DataState<AnyObject[]>;
export declare function isObjectDataState(test: any): test is DataState<AnyObject>;
export declare function isArrayPayload(test: any): test is {
    payload: AnyObject[];
};
export declare function isObjectPayload(test: any): test is {
    payload: AnyObject;
};
export declare function isStringPayload(test: any): test is {
    payload: string;
};
export declare function isStringArrayPayload(test: any): test is {
    payload: string[];
};
