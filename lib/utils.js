export function initDataState(data) {
    return {
        data,
        status: 'idle',
    };
}
/**
 * Redux toolkit's prepare function returning a Standard Flux Action with `meta` being our own `ActionMeta`.
 * https://github.com/redux-utilities/flux-standard-action
 */
export function createStandardFluxAction(payload, meta) {
    return {
        payload,
        error: false,
        meta: meta || {},
    };
}
export function testSetup() {
    const mockMeta = { idKey: 'id' };
    const obj1 = { id: 'testObj-1', value: false };
    const obj1Updated = Object.assign(Object.assign({}, obj1), { value: true });
    const obj2 = { id: 'testObj-2' };
    const obj3 = { id: 'testObj-3' };
    return {
        mockMeta,
        obj1,
        obj1Updated,
        obj2,
        obj3,
    };
}
