/* eslint-disable @typescript-eslint/no-explicit-any */
import { Dispatch,  Slice } from '@reduxjs/toolkit'

import {
  ActionMeta,
  ReducerOp,
  SpecializedSliceCaseReducers
} from '.';

export interface FetchHandlerConfig<State, FetchResult> {
  apiCall: (...args: any) => Promise<FetchResult>;
  slice: Slice<State, SpecializedSliceCaseReducers<State>>;
  meta: ActionMeta;
  apiCallArgs?: any[];
  cachingArgs?: any;
  reducer?: ReducerOp;
}

/**
 * TODO: Add documentation explaining:
 * 'Splitting' part: Split any fetch into Redux actions representing the three states requested, success, failure.
 * 'Caching' part: Deciding if fetch is actually executed based on timestamps and config params.
 */
export function fetchHandler<State, FetchResult>({
  apiCall,
  meta,
  slice,
  apiCallArgs = [],
  reducer = ReducerOp.Update,
}: FetchHandlerConfig<State, FetchResult>) {

  return async (dispatch: Dispatch) => {
    try {
      dispatch(slice.actions.setPending());
      const res = await apiCall(...apiCallArgs);
      if (reducer === ReducerOp.Update) {
        dispatch(slice.actions.update(res, meta));
      }
      else if (reducer === ReducerOp.Delete) {
        dispatch(slice.actions.remove(res))
      }

      return res;
    } catch (err) {
      dispatch(slice.actions.setFailed(err));
    }
  }
}