export { Draft } from '@reduxjs/toolkit';

export * from './fetchHandler';
export * from './makeSlice';
export * from './createRepo';

export enum ReducerOp {
  Update = 'Update',
  Delete = 'Delete',
}

export type Dictionary<T> = Record<string, T>;

/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export type AnyObject = Dictionary<any>;

export interface DataState<T> {
  data: T;
  status: 'idle' | 'pending';
  error?: Error;
  received?: number;
  requested?: number;
}

export type ActionMeta = {
  targetId?: string;
  targetIds?: string[];
  idKey?: string;
};

export function unpack<T>(dataState: DataState<T>) {
  return dataState.data;
}

type Processor = <Result>(fetchResult: unknown) => Result;
type ErrorHandler = (err: Error) => void;

export type ReduxIrlConfig = {
  /**
   * Will receive the result of a repo's fetch call. Return value will be passed to reducers.
   */
  defaultProcessor?: Processor;
  /**
   * Intercept fetch errors. A `/setFailed` action will still be fired to set the error property of `EntityState` in the
   * store. It might still be useful to handle all errors centrally.
   */
  errorHandler?: ErrorHandler;
}

class ReduxIrl {
  public defaultProcessor: Processor = <Result>(res: unknown) => res as Result;
  public errorHandler: ErrorHandler = (err) => { console.error('[redux-irl] Error: ', err) };

  init({
    defaultProcessor,
    errorHandler,
  }: ReduxIrlConfig) {
    if (defaultProcessor) {
      this.defaultProcessor = defaultProcessor;
    }
    if (errorHandler) {
      this.errorHandler = errorHandler;
    }
  }
}

const reduxIrl = new ReduxIrl();
export default reduxIrl;
