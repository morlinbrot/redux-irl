import { Draft } from '@reduxjs/toolkit';

import { DataState, FsaPayloadAction } from '../';


export * from './update';
export * from './remove';


export function setFailed<S extends DataState<S>>(state: S, action: FsaPayloadAction) {
  state.received = Date.now();
  state.status = 'idle';
  state.error = action.payload;
}

// export function setFailedSilently<T>(dataState: DataState<T>) {
//   return dataState.status = 'FailedSilently';
// }

export function setPending<S extends Draft<DataState<S>>>(state: S) {
  state.requested = Date.now();
  state.status = 'pending';
}

export function setIdle<S>(dataState: DataState<S>) {
  dataState.received = Date.now();
  dataState.status = 'idle';
}
